class CreateQuakes < ActiveRecord::Migration
  def change
    create_table :quakes do |t|
      t.string :guid
      t.string :title
      t.float :magnitude
      t.float :lat
      t.float :long
      t.datetime :occurred_on

      t.timestamps
    end

    add_index :quakes, :occurred_on
  end
end
