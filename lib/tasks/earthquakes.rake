require 'quakes'
namespace :earthquakes do

	desc "Synchronize earthquake information"
	task :sync => :environment do
		 # Quakes::Parser.new.execute('http://earthquake.usgs.gov/earthquakes/catalogs/eqs7day-M2.5.xml')
		Quakes::Parser.new.execute('http://earthquake.usgs.gov/earthquakes/catalogs/eqs1day-M0.xml')
	end
	
end