require 'nokogiri'
require 'open-uri'

module Quakes
	class Parser

		def execute(xml)
			read_file(xml) do |parsed_quake|
				quake = Quake.find_by_guid(parsed_quake.guid)
				if quake
					puts "********Existing entry found, updating attributes"
					quake.attributes = parsed_quake.attributes
				else
					quake = parsed_quake
				end

				quake.save
			end
		end

		protected
		def parse_entry(entry)
			quake = Quake.new
			quake.guid = entry['guid']
			return if not quake.guid
			
			quake.title = entry['title']
			quake.magnitude = entry['magnitude']
			quake.lat = entry['lat'].to_f
			quake.long = entry['long'].to_f
			quake.occurred_on = DateTime.parse(entry['occurred_on']) 

			quake
		end

		def read_file(xml)
			puts "******Parsing xml @  #{xml}"

			document = Nokogiri::XML(open(xml))

			quakes_data = document.xpath('//item').map do |q|
				quake = parse_entry({
					'guid' => q.xpath('guid').inner_text,
					'title' => q.xpath('title').inner_text,
					'magnitude' => q.xpath('title').inner_text.split(' ')[1].sub(',', ''),
					'lat' => q.xpath('geo:lat').inner_text,
					'long' => q.xpath('geo:long').inner_text,
					'occurred_on' => q.xpath('pubDate').inner_text
				})

				yield quake if quake
				puts "*******Parsed #{quake.guid}"
			end

			puts "******Finished parsing #{quakes_data.length} earthquakes"
		end
 
	end
end
