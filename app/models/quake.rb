class Quake < ActiveRecord::Base
  validates_presence_of :guid
  validates_uniqueness_of :guid

  scope :recent, lambda {|n=10| order('occurred_on DESC').limit(n) }
  scope :most_severe, order('magnitude DESC').limit(10)
  scope :past, lambda { |time| where('occurred_on >= ?', Time.now - time) }

  def self.get_by_time(filter)
		case filter
			when 'today'
				self.past(24.hours)
			when 'yesterday'
				self.past(48.hours)
			when 'last-week'
				self.past(7.days)
			else
				self.all
		end
  end
end