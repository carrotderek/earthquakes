class QuakesController < ApplicationController

  def index
    @quakes = Quake.all
    @most_severe = Quake.most_severe
    @recent = Quake.recent

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @quakes }
    end
  end

  def globe
    # @quakes = Quake.get_by_time(params[:past])
    @quakes = Quake.recent(300)

    data = []
    @quakes.each do |q|
      data << q.lat << q.long << q.magnitude
    end

    respond_to do |format|
      format.json { render json: data }
      format.js
    end
  end
end