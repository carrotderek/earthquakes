$(document).ready(function() {
  
  var globe = new DAT.Globe($('#globe')[0]);
  loadQuakes('globe.json');

  $('#quakes-all').click(function(e) {
    quakeLinkAction($(this), e, 'globe.json');
  });

  $('#quakes-day').click(function(e) {
    quakeLinkAction($(this), e, 'globe/today.json');
  });

  $('#quakes-yesterday').click(function(e) {
    quakeLinkAction($(this), e, 'globe/yesterday.json');
  });

  $('#quakes-week').click(function(e) {
    quakeLinkAction($(this), e, 'globe/last-week.json');
  });

  function quakeLinkAction(element, event, json) {
    event.preventDefault();
    element.parent().addClass('active').siblings().removeClass('active');
    loadQuakes(json);
  }

  function loadQuakes(json) {
    $('#globe').html('');
    var globe = new DAT.Globe($('#globe')[0]);
  	$.get('/quakes/'+json, function(quakes) {
  		globe.addData(quakes, {format: 'magnitude'});
  		globe.createPoints();
  		globe.animate();
  	});
  }
});