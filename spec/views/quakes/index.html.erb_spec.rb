require 'spec_helper'

describe "quakes/index" do
  before(:each) do
    assign(:quakes, [
      stub_model(Quake,
        :guid => "Quake",
        :title => "Title",
        :magnitude => 1.5,
        :lat => 1.5,
        :long => 1.5
      ),
      stub_model(Quake,
        :guid => "Quake",
        :title => "Title",
        :magnitude => 1.5,
        :lat => 1.5,
        :long => 1.5
      )
    ])
  end

  it "renders a list of quakes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Quake".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
